package ru.sber.jd.dto;

import lombok.Data;
import lombok.Setter;

@Data
@Setter
public class Apartment {
    private Home home;
    private String cadnum;

    public Apartment(Home home, String cadnum) {
        this.home= home;
        this.cadnum = cadnum;
    }
}
