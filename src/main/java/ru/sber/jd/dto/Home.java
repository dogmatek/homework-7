package ru.sber.jd.dto;

import lombok.Data;
import lombok.extern.log4j.Log4j2;

@Data
@Log4j2
public class Home {
    // Название комплекса
    private String name;
    // Квартиры входящие в комплекс
    private Apartment apartments[];

   public Home (){
       this.apartments = new Apartment[0];

   }

    public void addApartment(Apartment apartment){
        log.info("Размер apartments до начала добавления элемента = {}", this.apartments.length);

           Apartment tempApartments[] = new Apartment[this.apartments.length];

           for (int i = 0; i < this.apartments.length; i++ ) {
               tempApartments[i] = this.apartments[i];
           }

           this.apartments = new Apartment[this.apartments.length+1];
           for (int i = 0; i < tempApartments.length; i++ ) {
               this.apartments[i] = tempApartments[i];

           }
           this.apartments[tempApartments.length] = apartment;
            log.info("  Добавлен  apartment = {}", this.apartments[tempApartments.length].getCadnum());

           log.info("Размер apartments после добавления элемента = {}", this.apartments.length);
           log.info("");
                   /*
           Apartment apartments[] = new Apartment[this.apartments.length + 1];
           for (int i = 0; i < this.apartments.length; i++ ) {
                apartments[i+1] = this.apartments[i];
           }
           apartments[this.apartments.length] = apartment;
           this.apartments = apartments;
           */
    }


}
